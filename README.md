

## FLEX- Challenge


### Available Scripts

In the project directory, you can run:

### ` npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `dist` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!




### Table of contents
[Project structure](#project-structure)

[Installation](#installation)

[Configuration](#configuration)

[Technologies used](#technologies-used)

### Project structure

````

src/
|- components/                                # All components
|    |- Debts/ 
|       |- Delete.js _________________________ # Delete debts
        |- Form.js ___________________________ # Edit & Create debts
        |- index.js __________________________ # List all debts
|       |- styles.js _________________________ # Css modules styles
|    |- Main/ 
|       |- index.js __________________________ # List all client
|       |- styles.js _________________________ # Css modules styles
|- redux/      
    |- actions/
    |- user.js ______________________________ # User action for redux
      
    |- reducers/ 
       |- user.js __________________________ # User reducer
    |- service/   
        |- index.js _________________________ # all service
| 
````


### Installation

1- Clone the project

`git clone https://ferreiraguihh@bitbucket.org/ferreiraguihh/flex-challenge.git`

2-`npm install` to install npm packages

3- start dev server using `npm start`.

3- build and bundling your resources for production `npm run build`.


#### Technologies used

* [Webpack 4](https://github.com/webpack/webpack) 
* [Babel 8](https://github.com/babel/babel) [ transforming JSX and ES6,ES7,ES8 ]
* [React](https://github.com/facebook/react)
* [Redux](https://redux.js.org/)
* [CSS modules](https://github.com/css-modules/css-modules) [ Isolated style based on each component ] 
* [React-router 4.0](https://reacttraining.com/react-router/) 

Create by Guilherme Ferreira dos Santos
