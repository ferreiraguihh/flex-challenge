import axios from 'axios';
const TOKEN = '6c55c323-9b43-4b29-9a3b-603f7c5b6b96';
const URL = 'https://provadev.xlab.digital/api/v1/';

axios.interceptors.request.use((config) => {
    if(config.url.includes('jsonplaceholder')) {
        return config;
    }

    config.url = URL + config.url;
    config.params = {uuid: TOKEN}
    return config
})