export default function formatter(value, removeSymbol = false) {
    const valueFormated = parseInt((value || 0)).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
    return removeSymbol ? valueFormated.replace('R$', '') : valueFormated;
}
