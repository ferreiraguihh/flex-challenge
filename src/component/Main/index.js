import React from "react";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Client from "../Debts";
import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import * as actions from "../../redux/actions/user";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {bindActionCreators} from "redux";
import styles from "./styles";

class Main extends React.Component {
    state = {
        mobileOpen: false,
        open: false
    };

    componentDidMount() {
        this.props.listAllUser();
    }

    handleDrawerToggle = () => {
        this.setState(state => ({mobileOpen: !state.mobileOpen}));
    };

    handleClick = currentUser => {
        const {viewClient} = this.props;
        viewClient(currentUser);
        this.setState({open: true});
    };

    render() {
        const {classes, theme, users} = this.props;

        const drawer = (
            <div>
                <div className={classes.toolbar}/>
                <div className={classes.menuAjust}>
                    <Typography variant="title" color="primary">
                        Clientes
                    </Typography>
                </div>
                <List>
                    <Divider className={classes.dividerAjust}/>
                    {users.map(client => (
                        <ListItem button>
                            <ListItemIcon>
                                {" "}
                                <InboxIcon/>
                            </ListItemIcon>
                            <ListItemText
                                primary={client.name}
                                onClick={() => {
                                    this.handleClick(client);
                                }}
                            />
                        </ListItem>
                    ))}
                </List>
                <Divider/>
            </div>
        );

        return (
            <div className={classes.root}>
                <CssBaseline/>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" color="inherit" noWrap>
                            Flex relacionamentos inteligentes
                        </Typography>
                    </Toolbar>
                </AppBar>
                <nav className={classes.drawer}>
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            anchor={theme.direction === "rtl" ? "right" : "left"}
                            open={this.state.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper
                            }}
                            variant="permanent"
                            open
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
                {" "}
                <Fab color="primary" aria-label="Add" className={classes.fab}>
                    <AddIcon/>
                </Fab>
                <Client/>
            </div>
        );
    }
}

Main.propTypes = {
    classes: PropTypes.object.isRequired,
    users: PropTypes.arrayOf(PropTypes.object),
    listAllUser: PropTypes.func
};

export default connect(
    ({Client}) => ({
        users: Client.users
    }),
    dispatch => bindActionCreators(actions, dispatch)
)(withStyles(styles, {withTheme: true})(withRouter(Main)));
