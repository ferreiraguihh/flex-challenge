const drawerWidth = 240;

export default theme => ({
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      marginLeft: drawerWidth,
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
      },
    },
    menuButton: {
      marginRight: 20,
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
  
    drawerPaper: {
      width: drawerWidth,
    },
  
    menuAjust:{
      marginTop: 20, 
      marginLeft: 75
    },
    dividerAjust:{
      marginTop: 10
    },
  });