import Button from '@material-ui/core/Button/index';
import PropTypes from 'prop-types';
import React from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";


function Delete(props) {
    const {  cancelAction, confirmAction, open } = props;
    return (
        <Dialog
            open={open}
            onClose={cancelAction}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {"Tem certeza que quer deletar esta dívida?"}
            </DialogTitle>
            <DialogContent/>
            <DialogActions>
                <Button onClick={cancelAction} color="primary">
                    Não
                </Button>
                <Button
                    delete
                    onClick={confirmAction}
                    color="primary"
                    autoFocus
                >
                    Sim
                </Button>
            </DialogActions>
        </Dialog>
    );
}

Delete.propTypes = {
    classes: PropTypes.object.isRequired,
    cancelAction: PropTypes.func,
    confirmAction: PropTypes.func,
    open: PropTypes.bool.isRequired
};

export default Delete;

