import Button from '@material-ui/core/Button/index';
import Grid from '@material-ui/core/Grid/index';
import {withStyles} from '@material-ui/core/styles/index';
import TextField from '@material-ui/core/TextField/index';
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";

const styles = {
    card: {
        maxWidth: 445,
    },
};

class FormDebt extends Component {
    state = {
        debt: {}
    };

    componentWillReceiveProps({debt, open, currentUser}) {
        const newDebt = !debt._id ? ({ idUsuario: currentUser.id}) : debt;
        this.setState({debt: newDebt});
    }

    onChange = ({target}) => {
        const {value, name} = target;
        const {debt} = this.state;
        debt[name] = value;
        this.setState({debt});
    };


    render() {

        const {classes, currentUser, cancelAction, confirmAction, open,} = this.props;
        const {debt} = this.state;

        return (
            <Dialog
                open={open}
                onClose={cancelAction}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title"> {!!debt ? 'Edição de Divida' : "Nova dívida."}</DialogTitle>
                <DialogContent>
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item xs={12}>
                            <TextField
                                disabled
                                id="client"
                                label={currentUser.name}
                                className={classes.textField}
                                margin="normal"
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="motivo"
                                label="Motivo"
                                value={debt.motivo}
                                name={'motivo'}
                                onChange={this.onChange}
                                className={classes.textField}
                                margin="normal"
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="valor"
                                label="Valor"
                                type="number"
                                value={debt.valor}
                                name={'valor'}
                                onChange={this.onChange}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true
                                }}
                                margin="normal"
                                variant="outlined"
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={cancelAction} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={() => confirmAction(debt)} color="primary" autoFocus>
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

FormDebt.propTypes = {
    classes: PropTypes.object.isRequired,
    currentUser: PropTypes.object.isRequired,
    cancelAction: PropTypes.func,
    confirmAction: PropTypes.func,
    open: PropTypes.bool.isRequired,
    debt: PropTypes.object,
};

export default withStyles(styles)(FormDebt);
