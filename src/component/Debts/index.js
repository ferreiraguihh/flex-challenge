import React, {Fragment} from "react";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {withStyles} from "@material-ui/core/styles";
import * as actions from "../../redux/actions/user";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import NavigationIcon from "@material-ui/icons/Navigation";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import moment from "moment";
import Divider from "@material-ui/core/Divider";
import styles from "./styles";
import Form from "./Form";
import Delete from "./Delete";

import format from '../../service/currency';

class Debts extends React.Component {
    state = {
        openDelete: false,
        openForm: false,
        debt: {},
    };

    constructor(props) {
        super(props);
    }

    deleteDebts = () => {
        this.props.deleteDebts(this.state.debt);
        this.setState({openDelete: false})
    };

    handleOpenDelete = () => {
        const {openDelete} = this.state;
        this.setState({openDelete: !openDelete});
    };

    handleOpenForm = () => {
        const {openForm} = this.state;
        this.setState({openForm: !openForm});
    };

    saveForm = (debt) => {
         this.handleOpenForm();

        if(!!debt._id){
            this.props.updateDebt(debt);
            return;
        }
        this.props.saveDebt(debt);

    };

    render() {
        const {classes, currentUser} = this.props;
        const {openDelete, openForm, debt} = this.state;
        return (
            <Fragment>
                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    <Grid style={{fontSize: 19}}>
                    <Grid item xs={4}>
                        <Typography>Nome: {currentUser.name}</Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography>Email: {currentUser.email}</Typography>
                    </Grid>
                    </Grid>
                    <Divider/>
                    <Grid>
                        <Grid container item xs={12}>
                            {currentUser.debts.map(debt => (
                                <Card className={classes.card}>
                                    <CardContent>
                                        <Grid container className={classes.pd} key={debt}>
                                            <Grid
                                                container
                                                justify="space-between"
                                                alignItems="center"
                                            >
                                                <Grid container item xs={12} alignItems="center">
                                                    <Grid item xs={12}>
                                                        Motivo: {debt.motivo}
                                                    </Grid>
                                                </Grid>
                                                <Grid container item xs={12} className={classes.mt}>
                                                    <Grid item xs={6}>
                                                        Valor: {format(debt.valor)}
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        Criado: {moment(debt.criado).format("DD/MM/YYYY")}
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                    <CardActions>
                                        <Grid container justify="space-evenly">
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                onClick={() => {
                                                    this.setState({debt});
                                                    this.handleOpenDelete()
                                                }}
                                                className={classes.button}
                                            >
                                                Delete
                                                <DeleteIcon className={classes.rightIcon}/>
                                            </Button>
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                onClick={() => {
                                                    this.setState({debt});
                                                    this.handleOpenForm()
                                                }}
                                                className={classes.button}
                                            >
                                                Editar
                                                <NavigationIcon className={classes.extendedIcon}/>
                                            </Button>
                                        </Grid>
                                    </CardActions>
                                </Card>
                            ))}
                        </Grid>
                    </Grid>

                    <Delete open={openDelete} confirmAction={this.deleteDebts} cancelAction={this.handleOpenDelete}/>
                    <Form open={openForm} debt={debt} cancelAction={this.handleOpenForm}
                          confirmAction={this.saveForm}
                          currentUser={currentUser}/>
                </main>

                <main className={classes.content} style={{marginTop: 530}}>
                    <div className={classes.toolbar}/>
                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="flex-end"
                    >
                        <Fab color="primary" aria-label="Add" onClick={() => {
                            this.setState({debt: {}});
                            this.handleOpenForm()
                        }}>
                            <AddIcon/>
                        </Fab>
                    </Grid>
                </main>
            </Fragment>
        );
    }
}


Debts.propTypes = {
    classes: PropTypes.object.isRequired,
    currentUser: PropTypes.object,
    deleteDebts: PropTypes.func,
    saveDebt: PropTypes.func,
    updateDebt: PropTypes.func,
};

export default connect(
    ({Client}) => ({
        currentUser: Client.currentUser
    }),
    dispatch => bindActionCreators(actions, dispatch)
)(withStyles(styles)(withRouter(Debts)));
