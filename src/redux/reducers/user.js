import {DEBTS, DELETE_DEBT, LIST, VIEW, UPDATE_DEBT, ADD_DEBT} from "../constant";
import _ from 'lodash';

const initialState = {
    users: [],
    currentUser: {
        name: "",
        debts: [],
    },
};

const reducer = (state = initialState, action) => {


    if (action.type === LIST) {

        const users = action.users.map(user => ({...user, debts: []}));
        return {
            ...state,
            users,
        }
    }

    if (action.type === DEBTS) {
        const users = state.users.map((user) => {
            const debts = action.debts.filter((debt) => (user.id === debt.idUsuario));
            return ({
                ...user,
                debts,
            })
        });

        return {
            ...state,
            users,
            currentUser: (users.length > 0 ? users[0] : 0),
        }
    }

    if (action.type === VIEW) {
        return {
            ...state,
            currentUser: action.currentUser,
        }
    }


        if (action.type === DELETE_DEBT) {
            const {debtId} = action;
            let {currentUser} = state;

            currentUser.debts = currentUser.debts.filter(debt => (debt._id !== debtId));

            return {
                ...state,
                currentUser: _.cloneDeep(currentUser),
            }
        }

        if (action.type === ADD_DEBT) {
            const {debt} = action;
            const currentUser =_.cloneDeep(state.currentUser);
            currentUser.debts.push(debt);

            return {
                ...state,
                currentUser,
            }
        }

        if (action.type === UPDATE_DEBT) {

            const {debt} = action;
            const {currentUser} = state;
            const debts = currentUser.debts.map(current => {
                if (current._id === debt._id) {
                    return debt;
                }
                return current;
            });

            return ({
                ...state,
                currentUser: {
                    ...currentUser,
                    debts
                }
            })
        }


    return state;
};


export default reducer;


































