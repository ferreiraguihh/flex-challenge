export const DELETE_DEBT = 'DELETE_DEBT';
export const VIEW = 'VIEW';
export const DEBTS =  'DEBTS';
export const LIST =  'LIST';
export const ADD_DEBT = 'ADD_DEBT';
export const UPDATE_DEBT= 'UPDATE_DEBT';