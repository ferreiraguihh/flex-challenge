import axios from 'axios';
import {DEBTS, DELETE_DEBT, LIST, VIEW, ADD_DEBT, UPDATE_DEBT} from "../constant";

export const listAllUser = () => {
    return async (dispach) => {

        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        const result = response.data;
        dispach({
            type: LIST,
            users: result,
        });

        const  { data } =  await axios.get('divida');

        dispach({
            type: DEBTS,
            debts: data.result,
        })
    }
};



export const viewClient = (currentUser) => {
    return (dispach) => {
        dispach({
            type: VIEW,
            currentUser,
        })
    }
};

export const deleteDebts = (debt) => {
    return async (dispach)  => {
        await axios.delete('divida/'+ debt._id);

        dispach({
            type: DELETE_DEBT,
            debtId: debt._id,
        })


}};


export const saveDebt = (newDebt) => {
    return async (dispach)  => {
        const { data } = await axios.post('divida',{
            ...newDebt,
        });

        const {result} = data;
        debugger;
        dispach({
            type: ADD_DEBT,
            debt: {
                _id: result,
                ...newDebt,
            },
        })

    }};

export const updateDebt = (debt) => {
    return async (dispach)  => {
        await axios.put('divida/'+ debt._id, {
            ...debt
        });
        dispach({
            type: UPDATE_DEBT,
            debt,
        })

    }};